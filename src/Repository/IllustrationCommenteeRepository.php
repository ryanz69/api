<?php

namespace App\Repository;

use App\Entity\IllustrationCommentee;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method IllustrationCommentee|null find($id, $lockMode = null, $lockVersion = null)
 * @method IllustrationCommentee|null findOneBy(array $criteria, array $orderBy = null)
 * @method IllustrationCommentee[]    findAll()
 * @method IllustrationCommentee[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class IllustrationCommenteeRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, IllustrationCommentee::class);
    }

//    /**
//     * @return IllustrationCommentee[] Returns an array of IllustrationCommentee objects
//     */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('i')
            ->andWhere('i.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('i.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?IllustrationCommentee
    {
        return $this->createQueryBuilder('i')
            ->andWhere('i.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
